import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store';
import VueScrollactive from 'vue-scrollactive';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { dom } from '@fortawesome/fontawesome-svg-core'

library.add(fab)
Vue.component('font-awesome-icon', FontAwesomeIcon)
dom.watch()

Vue.use(VueScrollactive);

//const axios = require('axios').default;

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
