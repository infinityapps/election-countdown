export function menuScroll() {
  // When the user scrolls down 50px from the top of the document, resize the header's font size
  window.onscroll = function() {
    scrollFunction();
  };

  function scrollFunction() {
    if (
      document.body.scrollTop > 50 ||
      document.documentElement.scrollTop > 50
    ) {
      document.getElementById("header").style.padding = "0.5rem 2rem";
      document.getElementById("header").style.backgroundColor = "#212529";
      document.getElementById("brand").style.height = "3.5rem";
      document.getElementById("nav").style.fontSize = "0.9rem";
    } else {
      document.getElementById("header").style.padding = "2rem 4rem";
      document.getElementById("header").style.backgroundColor = "";
      document.getElementById("brand").style.height = "4.5rem";
      document.getElementById("nav").style.fontSize = "1rem";
    }
  }
}
